    <?php
        include_once'wp-config.php';//agregamos configuracion de la base de datos de wordpress

        function database(){//funcion que conecta a la base de datos
            $servername = DB_HOST;
            $database = DB_NAME;
            $username = DB_USER;
            $password = DB_PASSWORD;
            $conn = mysqli_connect($servername, $username, $password, $database);
            return $conn;
        }
         
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {//verificamos que el metodo de envio sea solo por POST
            $msg = 'Require method POST';
            $request = array('successful' => false, 'msg'=> $msg);
            return print_r(json_encode($request));
        }

        
        if (!empty($_POST['email'])) {//verificamos si el email contiene datos para procesar la informacion
            
            $user_email = $_POST['email'];

            $fecha = date("Y-m-d H:m:s");

            $subscriber = id_unico();

            $suscriptor =  strval('a:1:{s:10:"subscriber";b:1;}'); 
          
            $sql ="insert into ".$table_prefix."users (user_login ,user_nicename, user_email, user_registered, display_name)values('$subscriber','$subscriber','$user_email','$fecha','$subscriber');#insert into ".$table_prefix."usermeta (user_id, meta_key, meta_value)values((SELECT MAX(id) FROM ".$table_prefix."users),'".$table_prefix."capabilities','$suscriptor');";
            consulta($sql);

        }else{
            $msg = 'email empty';
            $request = array('successful' => false, 'msg'=> $msg);
            return print_r(json_encode($request));
        }

        
            

        function id_unico($dat = null){//funcion que genera un id unico para los suscriptores
            $entropy = true;
            $prefix  = trim('SUS');
            $length  = 20;
            if (isset($dat) && is_array($dat)) {
                $entropy = isset($dat['entropy']) ? (strtoupper($dat['entropy']) == 'FALSE' ? false : true) : $entropy;
                $prefix  = isset($dat['prefix']) ? strtoupper($dat['prefix']) : $prefix;
                $length  = isset($dat['length']) ? strtoupper($dat['length']) : $length;
            }

            $cad = strtoupper(uniqid($prefix, $entropy));
            $cad = str_replace(".", "", $cad);
            if ($prefix != '') {
                $cad = $prefix . substr($prefix.$cad, ((strlen($prefix) + 1) * 2), ($length-strlen($prefix)));
            } else {
                $cad = substr($cad, 0, $length);
            }
            return $cad;
        }
        
        function consulta($sql){//funcion que inserta en la base de datos wordpress
            $contador =0;

            $acceso = database();

            $sqls = explode("#", $sql);

           
            foreach ($sqls as $value) {

                if (!$acceso) {
                     die("Connection failed: " . mysqli_connect_error());
                }
                
                if (mysqli_query($acceso, $value)) {

                    $contador++;

                } else {
                    $msg = mysqli_error($acceso);
                    $request = array('successful' => false, 'msg'=> $msg);

                    return print_r(json_encode($request));
                }

                if ($contador==2) {
                    $request = array('successful' => true);
                    return print_r(json_encode($request));
                }

            }

            mysqli_close($acceso);

        }

    ?>